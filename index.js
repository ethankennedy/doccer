
// require hapi
var Hapi = require('hapi');

// instantiate a server
var server = new Hapi.Server('localhost', 8080, { cors: true });

// configure server to use handlebars
server.views({
    engines: {
        'html': require('handlebars')
    },
    path: './views',
    layout: 'index',
    layoutPath: './views/layouts'
});

// declare two routes
var routes = [
    {
        method   : 'GET',
        path     : '/',
        handler  : function(request, reply) {
            reply.view('index');
        }
    },
    {
        method   : 'POST',
        path     : '/',
        handler  : function(request, reply) {
        
            // enforce that we have required arguments
            var hasDataDown = request.payload.hasOwnProperty('template');
            var contextJSON = request.payload.hasOwnProperty('context') ? request.payload.context : '{}';
            var context     = JSON.parse(contextJSON);
                        
            if (!hasDataDown || typeof context != 'object') {
                throw {
                    name: 'DataDownError',
                    message: 'Template must be a DataDown string, and Context must be a valid JSON!'
                };
            }
            
            // require libs
            var phantom  = require('phantom');
            //var markdown = require('markdown').markdown;
            //var marked   = require('marked');
            var parser   = require('./parser.js')();

            // console.log('parser: ', parser);

            try {

                // get markdown
                var parsedMarkdown = parser.dataDownToMarkdown(request.payload.template, context);

                //console.log('parsedMarkdown: ', parsedMarkdown);

                // get html
                var html = parsedMarkdown; //marked(parsedMarkdown);

                //console.log('html: ', html);

                // open phantom and output html as pdf
                phantom.create(function(ph) {
                    ph.createPage(function(page) {
                        page.setContent(html, 'http://localhost/', function() {
                            page.set('paperSize', {
                              format: 'A4'
                            }, function() {
                                
                                require('crypto').randomBytes(16, function(ex, buf) {
                                    var token    = buf.toString('hex');
                                    var fileName = '/public/' + token + '.pdf';
                                    var url      = '/static/' + token + '.pdf';
                                    var filePath = process.cwd() + fileName;
                                
                                    //console.log('writing document to: ' + filePath);
                                
                                    page.render(filePath, function() {
                                        ph.exit();
                                        
                                        if (request.payload.hasOwnProperty('local')) {
                                            reply.file(filePath, {
                                                mode: 'inline'
                                            });
                                        }
                                        else {
                                            reply(url);
                                        }
                                    });
                                });
                            });
                        });
                    });
                });
            }
            catch (e) {
                console.log('Error generating document: ', e);
                throw e;
            }
        }
    },
    {
        method   : 'GET',
        path     : '/docs/new',
        handler  : function(request, reply) {
            reply.view('docs/new');
        }
    },
    {
        method   : 'GET',
        path     : '/static/{param*}',
        handler: {
            directory: {
                path: './public'
            }
        }
    },
    {
        method   : '*',
        path     : '/{p*}', 
        handler  : function(request, reply) {
            reply.view('404');
        }
    },
    {
        path: '/failure',
        method: 'GET',
        config: {
            handler: function(req) {
                req.reply('500');
            }
        }
    },
];

// route
server.route(routes);

// handle 500
server.on('internalError', function (request, err) {
    
    console.log('Error response (500) sent for request: ' + request.id + ' because: ' + err.message);
});

// start the server
server.start();

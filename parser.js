/*

    a module that takes a datadown string, together with a json payload and returns a markdown string.
    datadown is simply a special syntax for referencing data inside of markdown documents.
    in other words, this is a templating library that evaluates datadown templates against a given context (a json payload),
    and returns markdown. that markdown can then be rendered as html, which can then be rendered as pdf.

    pshahh!

*/

module.exports = function() {
    
    var Parser = {
        dataDown            : '',
        _dataDownInProgress : '',
        markdown            : '',
        rootContext         : {},
        data                : {},
        ifStatementMatrix   : [],
        eachStatementMatrix : [],
        expressionPattern   : /<%([\w. \*\/\+\[\]\{\}-]+?)%>/g
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    utility functions mimicking PHP-style internal array pointers, useful for building _if_ and _each_ matrices
    //
    //------------------------------------------------------------------------------------------------------------------------------
    function key(arr) {
      //  discuss at: http://phpjs.org/functions/key/
      // original by: Brett Zamir (http://brett-zamir.me)
      //    input by: Riddler (http://www.frontierwebdev.com/)
      // bugfixed by: Brett Zamir (http://brett-zamir.me)
      //        note: Uses global: php_js to store the array pointer
      //   example 1: array = {fruit1: 'apple', 'fruit2': 'orange'}
      //   example 1: key(array);
      //   returns 1: 'fruit1'

      this.php_js = this.php_js || {};
      this.php_js.pointers = this.php_js.pointers || [];
      var indexOf = function(value) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this[i] === value) {
            return i;
          }
        }
        return -1;
      };
      // END REDUNDANT
      var pointers = this.php_js.pointers;
      if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
      }

      if (pointers.indexOf(arr) === -1) {
        pointers.push(arr, 0);
      }
      var cursor = pointers[pointers.indexOf(arr) + 1];
      if (Object.prototype.toString.call(arr) !== '[object Array]') {
        var ct = 0;
        for (var k in arr) {
          if (ct === cursor) {
            return k;
          }
          ct++;
        }
        return false; // Empty
      }
      if (arr.length === 0) {
        return false;
      }
      return cursor;
    }
    
    function current(arr) {
      //  discuss at: http://phpjs.org/functions/current/
      // original by: Brett Zamir (http://brett-zamir.me)
      //        note: Uses global: php_js to store the array pointer
      //   example 1: transport = ['foot', 'bike', 'car', 'plane'];
      //   example 1: current(transport);
      //   returns 1: 'foot'

      this.php_js = this.php_js || {};
      this.php_js.pointers = this.php_js.pointers || [];
      var indexOf = function(value) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this[i] === value) {
            return i;
          }
        }
        return -1;
      };
      // END REDUNDANT
      var pointers = this.php_js.pointers;
      if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
      }
      if (pointers.indexOf(arr) === -1) {
        pointers.push(arr, 0);
      }
      var arrpos = pointers.indexOf(arr);
      var cursor = pointers[arrpos + 1];
      if (Object.prototype.toString.call(arr) === '[object Array]') {
        return arr[cursor] || false;
      }
      var ct = 0;
      for (var k in arr) {
        if (ct === cursor) {
          return arr[k];
        }
        ct++;
      }
      return false; // Empty
    }
    
    function next(arr) {
      //  discuss at: http://phpjs.org/functions/next/
      // original by: Brett Zamir (http://brett-zamir.me)
      //        note: Uses global: php_js to store the array pointer
      //   example 1: transport = ['foot', 'bike', 'car', 'plane'];
      //   example 1: next(transport);
      //   example 1: next(transport);
      //   returns 1: 'car'

      this.php_js = this.php_js || {};
      this.php_js.pointers = this.php_js.pointers || [];
      var indexOf = function(value) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this[i] === value) {
            return i;
          }
        }
        return -1;
      };
      // END REDUNDANT
      var pointers = this.php_js.pointers;
      if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
      }
      if (pointers.indexOf(arr) === -1) {
        pointers.push(arr, 0);
      }
      var arrpos = pointers.indexOf(arr);
      var cursor = pointers[arrpos + 1];
      if (Object.prototype.toString.call(arr) !== '[object Array]') {
        var ct = 0;
        for (var k in arr) {
          if (ct === cursor + 1) {
            pointers[arrpos + 1] += 1;
            return arr[k];
          }
          ct++;
        }
        return false; // End
      }
      if (arr.length === 0 || cursor === (arr.length - 1)) {
        return false;
      }
      pointers[arrpos + 1] += 1;
      return arr[pointers[arrpos + 1]];
    }
    
    function prev(arr) {
      //  discuss at: http://phpjs.org/functions/prev/
      // original by: Brett Zamir (http://brett-zamir.me)
      //        note: Uses global: php_js to store the array pointer
      //   example 1: transport = ['foot', 'bike', 'car', 'plane'];
      //   example 1: prev(transport);
      //   returns 1: false

      this.php_js = this.php_js || {};
      this.php_js.pointers = this.php_js.pointers || [];
      var indexOf = function(value) {
        for (var i = 0, length = this.length; i < length; i++) {
          if (this[i] === value) {
            return i;
          }
        }
        return -1;
      };
      // END REDUNDANT
      var pointers = this.php_js.pointers;
      if (!pointers.indexOf) {
        pointers.indexOf = indexOf;
      }
      var arrpos = pointers.indexOf(arr);
      var cursor = pointers[arrpos + 1];
      if (pointers.indexOf(arr) === -1 || cursor === 0) {
        return false;
      }
      if (Object.prototype.toString.call(arr) !== '[object Array]') {
        var ct = 0;
        for (var k in arr) {
          if (ct === cursor - 1) {
            pointers[arrpos + 1] -= 1;
            return arr[k];
          }
          ct++;
        }
        // Shouldn't reach here
      }
      if (arr.length === 0) {
        return false;
      }
      pointers[arrpos + 1] -= 1;
      return arr[pointers[arrpos + 1]];
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    main method for converting dataDown to Markdown
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.dataDownToMarkdown = function(dataDown, rootContext) {
        
        // dataDown must be a string
        if (typeof dataDown != 'string') throw { name: 'DataDownError', message: 'DataDown must be a string!' };
        
        // rootContext must be an object
        if (typeof rootContext != 'object') throw { name: 'RootContextError', message: 'RootContext must be a primitive object!' };
        
        // set our primary variables
        this.dataDown    = dataDown;
        this.rootContext = rootContext;
        
        try {
            
            // start parsing
            this.parseDataDown();            
        }
        catch (e) {
            console.log('Exception caught while parsing: ', e);
        }
        
        // return whatever we got
        return this.markdown;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    parseDataDown takes no arguments. it just calls each parsing method in succession.
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.parseDataDown = function() {
        
        // trim leading and trailing white space
        this.markdown = this.dataDown.trim();
        
        // first, we remove any unicode characters (e.g. tabs)
        this.removeUnicodeChars();
        
        // next, we look for _each_ loops
        this.parseEachLoops();
        
        // process any _each_ loops we've found
        this.processEachLoops();
        
        // now, look for any _if_ blocks
        this.parseIfBlocks();
        
        // process any _if_ blocks that we found
        this.processIfBlocks();
        
        // look for <img> tags
        this.parseImageTags();
        
        // look for [inline_variables]
        this.parseInlineVars();
        
        // process any number formatting
        this.formatNumbers();
        
        // process any date formatting
        this.formatDates();
        
        // process list joins _join_:['a', 'b', 'c'] = 'abc'
        this.processListJoins();
        
        // process line break tags
        this.processLineBreaks();
        
        // finally, process any remaining expressions
        this.processExpressions();
        
        // convert any single-quoted html attributes to double-quoted
        this.convertSingleQuoteAttributes();
        
        // trim leading and trailing white space
        this.markdown = this.markdown.trim();
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    removeUnicodeChars
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.removeUnicodeChars = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    parseEachLoops
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.parseEachLoops = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    processEachLoops
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.processEachLoops = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    parseIfBlocks
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.parseIfBlocks = function() {
        try {
            var markdownLines       = this.markdown.match(/\r\n/) ? this.markdown.split("\r\n") : this.markdown.split("\n");
            var ifConditionPattern  = /_if_:(.+)/;
            var ifElsePattern       = /_else_/;
            var ifCloseblockPattern = /_endif_/;
            var insideIfBlock       = false;
            var openIfBlockCount    = -1;
            var ifBlock             = [];
            
            for (var i = 0; i < markdownLines.length; i++) {
                var ifOpenMatches  = markdownLines[i].match(ifConditionPattern);
                var elseMatches    = markdownLines[i].match(ifElsePattern);
                var ifCloseMatches = markdownLines[i].match(ifCloseblockPattern);

                if (ifOpenMatches) {
                    if (!insideIfBlock) {
                        ifBlockCurrentIndex = key(ifBlock);
                        insideIfBlock       = true;
                        openIfBlockCount    = 0;

                        var block = {
                            'line_number'   : i,
                            'if_condition'  : ifOpenMatches[1],
                            'if_block'      : "",
                            'else_block'    : "",
                            'contains_each' : false,
                            'is_closed'     : false,
                        };
                        
                        ifBlock.push(block);

                        markdownLines[i] = "_ifblock_" + i;

                    }
                    else {
                        ifBlockCurrentIndex = key(ifBlock);
                        ifBlock_i = ifBlockCurrentIndex;

                        while (ifBlock[ifBlock_i]['is_closed'] && ifBlock_i > 0) {
                            ifBlock_i--;
                        }

                        ifBlock[ifBlock_i]['if_block'] += "_ifblock_" + i + "\n";
                        openIfBlockCount++;

                        var block = {
                            'line_number'   : i,
                            'if_condition'  : ifOpenMatches[1],
                            'if_block'      : "",
                            'else_block'    : "",
                            'contains_each' : false,
                            'is_closed'     : false,
                        };
                        
                        ifBlock.push(block);

                        next(ifBlock);
                        markdownLines[i] = '';
                    }
                }
                else if (ifCloseMatches) {

                    ifBlockCurrentIndex = key(ifBlock);
                    ifBlock_i = ifBlockCurrentIndex;

                    while (ifBlock[ifBlock_i]['is_closed'] && ifBlock_i > 0) {
                        ifBlock_i--;
                    }

                    ifBlock[ifBlock_i]['is_closed'] = true;
                    openIfBlockCount--;

                    if (openIfBlockCount == -1) {
                        insideIfBlock             = false;
                        this.ifStatementMatrix.push(ifBlock);
                        ifBlock                   = [];
                    }

                    markdownLines[i] = '';
                }
                else if (insideIfBlock) {

                    ifBlockCurrentIndex = key(ifBlock);

                    ifBlock_i = ifBlockCurrentIndex;

                    while (ifBlock[ifBlock_i]['is_closed'] && ifBlock_i > 0) {
                        ifBlock_i--;
                    }

                    // if (this.hasEachOpenStatement(markdownLines[i])) {
                    //     ifBlock[ifBlock_i]['contains_each'] = true;
                    // }

                    ifBlock[ifBlock_i]['if_block'] += markdownLines[i] + "\n";
                    markdownLines[i] = '';
                }
            }
            
            this.markdown = markdownLines.join("\n");
                        
        }
        catch (e) {
            console.log("parser.js: error parsing if blocks ", e);
        }
        
        console.log('parser.js parseIfBlocks ifStatementMatrix: ', this.ifStatementMatrix);
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    processIfBlocks
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.processIfBlocks = function() {
        for (var i = 0; i < this.ifStatementMatrix.length; i++) {
            var ifBlock = this.ifStatementMatrix[i];
            
            for (var j = 0; j < ifBlock.length; j++) {
                var if_statement       = ifBlock[j];
                var if_condition_count = 0;
                var if_vars            = [];
                var if_output_result   = "";
                var is_true            = this.evaluateIf(if_statement['if_condition']);
                var else_components    = if_statement['if_block'].split('_else_');

                console.log('if_condition: ', if_statement['if_condition']);
                console.log('is_true: ', is_true);
                
                if (!is_true && else_components.length > 1) {
                    if_output_result = else_components[1];
                }
                else if (is_true) {
                    if_output_result = else_components[0];
                }
            
                this.markdown = this.markdown.replace("_ifblock_" + if_statement['line_number'] + "\n", if_output_result);
            }
        }
        
        return true;
    };
    
    Parser.evaluateIf = function(expression) {
        var if_operands = [
            ' == ', // equals
            ' >> ', // greater than
            ' << ', // less than
            ' != ', // not equal
            ' _has_ ', //if term1 is array, returns true if term2 is in array; otherwise, term1 is treated as a string and true is returned if term2 is a subtring of term1
        ];
        
        var stand_alone_if_expression = true;
        var operand_in_use            = '';
        
        for (var i = 0; i < if_operands.length; i++) {
            var operand = if_operands[i];
            
            if (expression.indexOf(operand) != -1) {
                stand_alone_if_expression = false;
                operand_in_use = operand;
            }
        }
        
        if (stand_alone_if_expression) {
            console.log("DocumentTemplateBuilder: evaluateIf, stand_alone_if_expression {expression}");
            parsed_expression = this.resolveExpression(expression, false, true);
        }
        else {
            
            expression_terms = expression.split(operand_in_use);
            
            if (expression_terms.length != 2) {
                throw { name: 'SyntaxError', message: "If comparison condition must be of the simple form <term1> == <term2>: " + expression };
            }
            
            term1              = expression_terms[0].trim();
            term2              = expression_terms[1].trim();
            expression_matches = term1.match(this.expressionPattern);
            
            if (expression_matches) {
                term1 = this.resolveExpression(term1, false, true);
            }
            
            // before we attempt to parse term2, check for special null operator
            if (term2.indexOf('_null_') != -1) {
                switch (operand_in_use) {
                    case ' == ':
                        return (term1 == null);
                    case ' != ':
                        return (term1 != null);
                }
            }

            //*****************************************************************************************
            //
            //              update _empty_ operator
            //
            //*****************************************************************************************
            // before we attempt to parse term2, check for special empty operator
            // if (term2.indexOf('_empty_') != -1) {
            //     switch (operand_in_use) {
            //         case ' == ':
            //             return empty(term1);
            //         case ' != ':
            //             return !empty(term1);
            //     }
            // }

            expression_matches = term2.match(this.expressionPattern);
            
            if (expression_matches) {
                term2 = this.resolveExpression(term2, false, true);
            }
            
            console.log('Parser::evaluateIf() term1: ', term1);
            console.log('Parser::evaluateIf() term2: ', term2);
            
            switch (operand_in_use) {   
                case ' == ':
                    parsed_expression = (term1 == term2);
                    break;
                case ' >> ':
                    parsed_expression = (term1 > term2);
                    break;
                case ' << ':
                    parsed_expression = (term1 < term2);
                    break;
                case ' != ':
                    parsed_expression = (term1 != term2);
                    break;
                case ' _has_ ':

                    if (typeof term1 == 'array') {
                        parsed_expression = !!(term1.indexOf(term2) != -1);
                    }
                    else {
                        term1_str         = term1.toString();
                        term2_str         = term2.toString();
                        
                        console.log('Parser::evaluateIf(' + expression + ') term1 as string; term2: ' + term2_str + '; term1: ' + term1_str);
                        
                        parsed_expression = !!(term1.indexOf(term2) != -1);
                    }
                    
                    break;
                default:
                    parsed_expression = (term1 == term2);
            }
        }
        
        return (!!parsed_expression);
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    parseImageTags
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.parseImageTags = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    parseInlineVars
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.parseInlineVars = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    formatNumbers
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.formatNumbers = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    formatDates
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.formatDates = function() {
        
        // wrap in a try catch because that's what god wants
        try {
            
            // initialize are regex and our match buckets
            var format_date_pattern = /_(date|datetime)_:(.+)/;
            var date_format_pattern = /\[([^\]]+)\]/;
            var date_matches        = [];
            var moment              = require('moment');
            
            // find any strings in our markdown of the format _date_:<% expression %>[date-format][timezone-override]
            while (date_matches = this.markdown.match(format_date_pattern)) {
                
                // ensure that we have 3 terms in our match array
                if (date_matches.length == 3) {
                    var dateFunction = date_matches[1];
                    var dateSrc      = date_matches[2];
                    var dateFormat   = dateSrc.match(date_format_pattern);
                    
                    // check whether a format string was passed
                    if (!dateFormat) {
                        dateFormat = ['', 'MM/DD/YYYY'];
                    }
                    
                    // parse expression
                    var expression = dateSrc.match(this.expressionPattern);
                    
                    if (expression) {
                        var expressionValue = this.resolveExpression(expression[0]);
                        
                        if (expressionValue == '' || expressionValue == 'now' || expressionValue == 'today') {
                            var dateReplace = moment().format(dateFormat[1]);
                        }
                        else {
                            var dateReplace = moment(expressionValue).format(dateFormat[1]);
                        }
                        
                        this.markdown       = this.markdown.replace(date_matches[0], dateReplace);
                    }
                }
            }            
        }
        catch (e) {
            console.log("Parser::formatDates() exception: ", e);
        }
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    processListJoins
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.processListJoins = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    processLineBreaks
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.processLineBreaks = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    processExpressions
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.processExpressions = function() {
        var expressionMatches = this.markdown.match(this.expressionPattern)
        
        if (expressionMatches) {
            for (var i = 0; i < expressionMatches.length; i++) {
                var getArrayAsReturnValue        = false;
                var forceEvaluationOfFieldValues = true;
                var parsedExpression             = this.resolveExpression(expressionMatches[i], getArrayAsReturnValue, forceEvaluationOfFieldValues);
                this.markdown                    = this.markdown.replace(expressionMatches[i], parsedExpression);
            }
        }
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    convertSingleQuoteAttributes
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.convertSingleQuoteAttributes = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    parseMath
    //
    //------------------------------------------------------------------------------------------------------------------------------
    Parser.parseMath = function() {
        this.markdown = this.markdown;
        
        return true;
    };
    
    //------------------------------------------------------------------------------------------------------------------------------
    //
    //    this is the primary method for resolving data references in the datadown string
    //
    //------------------------------------------------------------------------------------------------------------------------------
    function isEmpty(obj) {
        console.log('isEmpty: typeof obj - ', typeof obj);
        
        if (typeof obj != 'object'){
            return false;
        }
        
        for (prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                return false;
            }
        }
        
        return true;
    }
    
    Parser.resolveExpression = function(expression) {
        
        // console.log('expression: ', expression);
        
        // expression must be a string
        if (typeof expression != 'string') throw { name: 'DataDownError', message: 'Expression must be a string!' };
        
        // if additional, optional arguments are getArrayAsReturnValue and forceEvaluationOfFieldValues
        var getArrayAsReturnValue        = (arguments.length == 2) ? !!(arguments[1]) : false;
        var forceEvaluationOfFieldValues = (arguments.length == 3) ? !!(arguments[2]) : false;
        
        // we need to keep track of our scope as we traverse the terms of the expression
        var scope = {};
        
        // first trim <% and %>, as well as newlines and spaces off of the expression string if we need to
        expression = expression.replace(/<|%|\s|>/g, '');
        
        // console.log('expression: ', expression);
        
        // split expression into terms
        var terms = expression.split('.');
        
        // console.log('terms: ', terms);
        console.log('rootContext: ', this.rootContext);
        
        // loop thru terms and continually update scope
        for (var i = 0; i < terms.length; i++) {
            try {
                console.log('terms[i]: ' + terms[i]);
                
                if (!i && this.rootContext.hasOwnProperty(terms[i])) {
                    scope = this.rootContext[terms[i]];
                }
                else if (terms[i] == 'length') {
                    scope = scope.length;
                    break;
                }
                else if (scope.hasOwnProperty(terms[i])) {
                    scope = scope[terms[i]];
                }
            }
            catch (e) {
                console.log('error resolving terms: ', e);
                return scope.toString();
            }
            
            console.log('scope: ', scope);
        }
        
        // if scope is still an empty object, assume expression is just a literal and return it
        if (isEmpty(scope)) {
            console.log('scope is empty: ', scope);
            return expression;
        }
        else {
            return scope;
        }
    };
    
    return Parser;
};